#### `section3`

# 반응속도 게임을 통해 setTimeout을 리액트에 적용하기.

> ## React 조건문

- 리액트 작동에서는 자바스크립트 영역만 작성 할 것.

* (style은 html 에 넣어두 됨.)

* return 안에서 if와 for를 사용 할 수 없다.

* `result: [],` 빈배열 일 떄는 reduce를 사용할 수 없다.
* false,undefined,null 은 jsx 에서 태그없음을 의미함.

✅

```
{this.state.result.length === 0
? null
: <tag></tag>
}
```

- **조건문을 사용할 시, 삼항연산자를 이용한다 ( 가독성이 떨어짐)**  
  (너무 지저분해질 시, render()위로 빼준다)

> ## setTimeout 넣어 반응속도 체크하기

- 클래스환경에서 this.state는 구조분해를 미리 할 것,(자주 사용하기때문에 깔끔해짐)

* setTimeout 활용법

```
setTimeout(()=>{
  this.setState({
    state:'now',
    message:'click now',
  })
})
```

- setTimeout은 여기서도 그냥 실행된다...

**해결법**

```
//this.timeout을 사용한다.
1.timeout; 선언.
2. setTimeout 을 this.timeout 에 넣어준다.
3. clearTimeout(this.timeout);으로 작성한다. //setTimeout 작동중지
```

- startTime 과 endTime을 설정하여 반응속도를 구한다.
- 이때, state안에 작성하지 말것(리렌더링 하면안댐)

* setTimeout이 콜스택으로 넘어가더라도 clearTimeout은 취소를 할 수 있다.

> ## HOOKS 로 전환하기

- this , state 다 없애주기

- 이때, 한가지 애매한건 this.startTime, this.endTime

* 예전state를 참고할때는 setState를 함수로 작성해준다 (prevState)

* Hooks에서는 this의 속성들을 Ref로 작성한다. (Ref의 추가적인 기능)  
  `const timeout = useRef(null);`  
  `const startTime =useRef(null);`  
  `const endTIme =useRef(null);`

  ```
  timeout.current = setTimeout(()=>{})
  startTime.current = new Date();
  endTime.current = new Date();
  ```

* ref에 대입하는건, 렌더링이 되지 않는다 (변하는 값을 잠깐 기록)  
  보통 setTimeout,interval 은 ref에 대입한다.

* 알다시피 state는 렌더링에 관여.

* **ref는 무조건 current 붙이기**

> ## return 내부에 for문과 if문 사용 해보기

- 잘 안씀 많이 지저분해짐

{} 중괄호를 통해서 소스코드를 작성,

if문은 함수안에서 작성가능하므로

```
{()=>{
if(){ 함수 조건문
return
}
}}

//을 작성뒤 즉시실행함수(IIFE)로 바꿔준다
{(()=>{
if(){ 함수 조건문
return
}
})()}
```

for문 역시 함수안에서 작성 가능 하므로,

```
{()=>{
  for() {

  }
}}

//을 작성뒤 즉시실행함수로 감싸준다.
{(()=>{
  const array =[];
  for() {
array.push(jsx태그);ㄴ
  }
  return array;
})()}
```

이렇게 사용할꺼면 1. 함수를 위로 빼기, 2. 컴포넌트로 빼기

- **배열안에다가 jsx를 담아서 리턴하는거 유효한 문법임**
