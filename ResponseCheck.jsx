import React, { Component } from "react";

class ResponseCheck extends Component {
  state = {
    state: "waiting",
    message: "click to start",
    result: [],
  };

  onClickScreen = () => {};

  render() {
    return (
      <>
        <div
          id="screen"
          className={this.state.state}
          onClick={this.onClickScreen}>
          {this.state.message}
        </div>
        <div>
          평균시간 :{this.result.reduce((a, c) => a + c) / this.result.length}
          ms
        </div>
      </>
    );
  }
}

export default ResponseCheck;
